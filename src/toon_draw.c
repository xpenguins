/* toon_draw.c - draw and erase the toons 
 * Copyright (C) 1999-2001  Robin Hogan
 * Copyright (C) 2010  James Hogan <james@albanarts.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include <stdio.h>
#include <stdlib.h>
#include "toon.h"

/* exposing and recapturing areas of screen */

#define EXPOSE_PROB 30
#define EXPOSE_DIRECTION 90
#define EXPOSE_UNIFORMITY 1
#define EXPOSE_HISTORY_LENGTH 5
/*#define EXPOSE_DEBUG*/

static int exposing = 0;
static int expose_bounds[2][2];

static int expose_history_start = 0;
static int expose_history_length = 0;
static int expose_history[EXPOSE_HISTORY_LENGTH][2][2];

int *
ToonExposingHistoryFetch(int id)
{
  int index = (expose_history_start + id) % EXPOSE_HISTORY_LENGTH;
  return &expose_history[index][0][0];
}

int
ToonExposingHistoryPop()
{
  if (expose_history_length) {
    ++expose_history_start;
    --expose_history_length;
  }
}

int
ToonExposingHistoryPush()
{
  int *item;

  if (expose_history_length >= EXPOSE_HISTORY_LENGTH) {
    ToonExposingHistoryPop();
  }

  item = ToonExposingHistoryFetch(expose_history_length);
  item[0] = expose_bounds[0][0];
  item[1] = expose_bounds[0][1];
  item[2] = expose_bounds[1][0];
  item[3] = expose_bounds[1][1];
  ++expose_history_length;
}

#define RandInt(maxint) ((int) ((maxint)*((float) rand()/(RAND_MAX+1.0))))

/* adjust bounds to exclude the specified rectangle */
int
ToonExposingBound(int *pos, int *rect_pos, int *rect_size, int flex)
{
  int rect_cent[2] = { rect_pos[0] + rect_size[0]/2,
                       rect_pos[1] + rect_size[1]/2 };
  int greater, min, max;
  int retry = 0;

  /* in which direction are we furthest from (x, y)? */
  greater = (abs(rect_cent[0] - pos[0]) <= abs(rect_cent[1] - pos[1]));
try_again:
  min = rect_pos[greater] - flex;
  max = rect_pos[greater] + rect_size[greater] + flex;
  /* shrink the bounds */
  if (min > pos[greater]) {
    if (min < expose_bounds[greater][1]) {
      expose_bounds[greater][1] = min;
    }
  }
  else if (max < pos[greater]) {
    if (max > expose_bounds[greater][0]) {
      expose_bounds[greater][0] = max;
    }
  }
  else if (!retry) {
    retry = 1;
    greater = !greater;
    goto try_again;
  }
  else {
    return 1;
  }
  return 0;
}

void
ToonExposingExpose(Toon *t, int n)
{
  const int win_flex = 0;
  const int toon_flex = 3;

  static int burst_countdown = 0;
  int attempts = 100;
  int expose_prob = EXPOSE_PROB;
  int pos[2];
  int i, j;
  XExposeEvent expose;

  static int last_desktop = -2;
  int current_desktop = -1;

  if (exposing || !toon_root_copy_obtained) {
    return;
  }

  current_desktop = ToonCurrentDesktop();
  if (last_desktop == -2) {
    last_desktop = current_desktop;
  }
  else if (last_desktop != current_desktop) {
    /* desktop has changed, do exposes for the next 200 frames */
    burst_countdown = 200;
    last_desktop = current_desktop;
  }

  if (burst_countdown) {
    expose_prob = 100;
    expose_history_length = 0;
    --burst_countdown;
  }

  if (RandInt(100) >= expose_prob) {
    return;
  }

try_again:
  if (!--attempts) {
    ToonExposingHistoryPop();
    return;
  }

  expose_bounds[0][0] = 0;
  expose_bounds[0][1] = ToonDisplayWidth();
  expose_bounds[1][0] = 0;
  expose_bounds[1][1] = ToonDisplayHeight();

  /* Choose a random location on the screen */

  if (n) {
    i = RandInt(n);
    j = RandInt(8+EXPOSE_UNIFORMITY);
#ifdef EXPOSE_DIRECTION
    if (j < 4 && RandInt(100) <= EXPOSE_DIRECTION) {
      switch (t[i].direction) {
        case TOON_LEFT:
          j = 1;
          break;
        case TOON_RIGHT:
          j = 0;
          break;
        case TOON_UP:
          j = 3;
          break;
        case TOON_DOWN:
          j = 2;
          break;
        case TOON_UPLEFT:
          j = 7;
          break;
        case TOON_UPRIGHT:
          j = 6;
          break;
        case TOON_DOWNLEFT:
          j = 5;
          break;
        case TOON_DOWNRIGHT:
          j = 4;
          break;
        default:
          break;
      }
    }
#endif
  }
  else {
    j = 4;
  }
  switch (j) {
    case 0: /* left */
      pos[0] = t[i].x - toon_flex - 1;
      pos[1] = t[i].y + t[i].height_map/2;
      break;
    case 1: /* right */
      pos[0] = t[i].x + t[i].width_map + toon_flex + 1;
      pos[1] = t[i].y + t[i].height_map/2;
      break;
    case 2: /* top */
      pos[0] = t[i].x + t[i].width_map/2;
      pos[1] = t[i].y - toon_flex - 1;
      break;
    case 3: /* bottom */
      pos[0] = t[i].x + t[i].width_map/2;
      pos[1] = t[i].y + t[i].height_map + toon_flex + 1;
      break;
    case 4: /* top left */
      pos[0] = t[i].x - toon_flex - 1;
      pos[1] = t[i].y - toon_flex - 1;
      break;
    case 5: /* top right */
      pos[0] = t[i].x + t[i].width_map + toon_flex + 1;
      pos[1] = t[i].y - toon_flex - 1;
      break;
    case 6: /* bottom left */
      pos[0] = t[i].x - toon_flex - 1;
      pos[1] = t[i].y + t[i].height_map + toon_flex + 1;
      break;
    case 7: /* bottom right */
      pos[0] = t[i].x + t[i].width_map + toon_flex + 1;
      pos[1] = t[i].y + t[i].height_map + toon_flex + 1;
      break;
    default: /* uniform random */
      pos[0] = RandInt(ToonDisplayWidth());
      pos[1] = RandInt(ToonDisplayHeight());
      break;
  }

  /* Find fairly maximal rectangle around pos */

  if (toon_windata) {
    for (i = 0; i < toon_nwindows; ++i) {
      int win_pos[2] = { t->x, t->y };
      int win_size[2] = { t->width_map, t->height_map };
      if (ToonExposingBound(pos, win_pos, win_size, win_flex)) {
        goto try_again;
      }
    }
  }

  for (i = 0; i < expose_history_length; ++i) {
    int *rect = ToonExposingHistoryFetch(i);
    int rect_pos[2] = { rect[0], rect[2] };
    int rect_size[2] = { rect[1] - rect[0], rect[3] - rect[2] };
    if (ToonExposingBound(pos, rect_pos, rect_size, 0)) {
      goto try_again;
    }
  }

  for (i = 0; i < n; ++i, ++t) {
    int toon_pos[2] = { t->x, t->y };
    int toon_size[2] = { t->width_map, t->height_map };
    if (ToonExposingBound(pos, toon_pos, toon_size, toon_flex)) {
      goto try_again;
    }
  }

  /* Trigger the actual expose event */

  expose.type        = Expose;
  expose.send_event  = True;
  expose.display     = toon_display;
  expose.window      = toon_root;
  expose.x           = expose_bounds[0][0];
  expose.y           = expose_bounds[1][0];
  expose.width       = expose_bounds[0][1] - expose_bounds[0][0] + 1;
  expose.height      = expose_bounds[1][1] - expose_bounds[1][0] + 1;
  XSendEvent(toon_display, toon_root, True, Expose,
             (XEvent *) &expose);

  ToonExposingHistoryPush();

  exposing = 1;
}

int
ToonExposingRecapture()
{
  /* recapture exposed region */

  if (toon_root_copy_obtained && exposing) {
    XCopyArea(toon_display, toon_root, toon_root_copy, toon_drawGC,
              expose_bounds[0][0],
              expose_bounds[1][0],
              expose_bounds[0][1] - expose_bounds[0][0],
              expose_bounds[1][1] - expose_bounds[1][0],
              expose_bounds[0][0],
              expose_bounds[1][0]);
    exposing = 0;

#ifdef EXPOSE_DEBUG
    XDrawRectangle(toon_display, toon_root, toon_drawGC,
                   expose_bounds[0][0],
                   expose_bounds[1][0],
                   expose_bounds[0][1] - expose_bounds[0][0],
                   expose_bounds[1][1] - expose_bounds[1][0]);
#endif
  }
}

/* DRAWING FUNCTIONS */

/* Draw the toons from toon[0] to toon[n-1] */
/* Currently always returns 0 */
int
ToonDraw(Toon *toons, int n)
{
  int i;
  Toon *t = toons;
  for (i = 0; i < n; i++, t++) {
    if (t->active) {
      ToonData *data = toon_data[t->genus] + t->type;
      int width = data->width;
      int height = data->height;
      int direction = t->direction;
      if (direction >= data->ndirections) {
	direction = 0;
      }

      XSetClipOrigin(toon_display, toon_drawGC,
		     t->x-width*t->frame, t->y-height*direction); 
      XSetClipMask(toon_display, toon_drawGC, data->mask);   
      XCopyArea(toon_display, data->pixmap,
		toon_root, toon_drawGC, width*t->frame, height*direction,
		width, height, t->x, t->y);
      XSetClipMask(toon_display, toon_drawGC, None);
      t->x_map = t->x;
      t->y_map = t->y;
      t->width_map = width;
      t->height_map = height;
      t->genus_map = t->genus;
      t->type_map = t->type;
      t->frame_map = t->frame;
      t->direction_map = direction;
      t->mapped = 1;
    }
    else {
      t->mapped = 0;
    }
  }
  
  ToonExposingExpose(toons, n);

  return 0;
}

/* Erase toons toon[0] to toon[n-1] */
/* Currently always returns 0 */
/* If toon_expose is set then every 100th frame an expose event will
 * be sent to redraw any desktop icons */
int
ToonErase(Toon *t, int n)
{
  static int minx = 10000;
  static int maxx = 0;
  static int miny = 10000;
  static int maxy = 0;
  static int count = 0;

  int i;
  
  ToonExposingRecapture();

  for (i = 0; i < n; i++, t++) {
    if (t->mapped) {
      int x = t->x_map;
      int y = t->y_map;
      int width = t->width_map;
      int height = t->height_map;
      int frame = t->frame_map;
      int direction = t->direction_map;
      int genus = t->genus_map;
      int type = t->type_map;
      if (toon_root_copy_obtained) {
        ToonData *data = toon_data[genus] + type;
        XSetClipOrigin(toon_display, toon_drawGC,
                       x - width*frame, y - height*direction); 
        XSetClipMask(toon_display, toon_drawGC, data->mask);   
        XCopyArea(toon_display, toon_root_copy, toon_root, toon_drawGC,
                  x, y, width, height, x, y);
        XSetClipMask(toon_display, toon_drawGC, None);
      }
      else {
        XClearArea(toon_display, toon_root, x, y,
                   width, height, False);
      }
      if (toon_expose) {
	if (x < minx) {
	  minx = x;
	}
	if (x + width > maxx) {
	  maxx = x + width;
	}
	if (y < miny) {
	  miny = y;
	}
	if (y + height > maxy) {
	  maxy = y + height;
	}
      }
    }
  }

  if (toon_bgcapture && !toon_root_copy_obtained) {
    toon_root_copy = XCreatePixmap(toon_display, toon_root,
                                   toon_display_width, toon_display_height,
                                   toon_display_depth);
    XCopyArea(toon_display, toon_root, toon_root_copy, toon_drawGC,
              0, 0, toon_display_width, toon_display_height,
              0, 0);
    toon_root_copy_obtained = 1;
  }

  if (!toon_root_copy_obtained
      && toon_expose && count > 100
      && maxx > minx && maxy > miny) {
    XExposeEvent event;

    event.type        = Expose;
    event.send_event  = True;
    event.display     = toon_display;
    event.window      = toon_root;
    event.x           = minx;
    event.y           = miny;
    event.width       = maxx-minx + 1;
    event.height      = maxy-miny + 1;
    XSendEvent(toon_display, toon_root, True, Expose,
	       (XEvent *) &event);
    minx = 10000;
    maxx = 0;
    miny = 10000;
    maxy = 0;
    count = 0;
  }
  else {
    ++count;
  }

  return 0;
}

/* Send any buffered X calls immediately */
void
ToonFlush()
{
  XFlush(toon_display);
  return;
}
