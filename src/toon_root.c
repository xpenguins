/* toon_root.c - finding the correct background window / virtual root
 * Copyright (C) 1999-2001  Robin Hogan
 * Copyright (C) 2010-2012  James Hogan <james@albanarts.com>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Since xpenguins version 2.1, the ToonGetRootWindow() function
 * attempts to find the window IDs of
 *
 * 1) The background window that is behind the toplevel client
 *    windows; this is the window that we draw the toons on.
 *
 * 2) The parent window of the toplevel client windows; this is used
 *    by ToonLocateWindows() to build up a map of the space that the
 *    toons can occupy.
 * 
 * In simple (sensible?) window managers (e.g. blackbox, sawfish, fvwm
 * and countless others), both of these are the root window. The other
 * more complex scenarios that ToonGetRootWindow() attempts to cope
 * with are:
 *
 * Some `virtual' window managers (e.g. amiwm, swm and tvtwm) that
 * reparent all client windows to a desktop window that sits on top of
 * the root window. This desktop window is easy to find - we just look
 * for a property __SWM_VROOT in the immediate children of the root
 * window that contains the window ID of this desktop window. The
 * desktop plays both roles (1 and 2 above). This functionality was
 * detected in xpenguins 1.x with the vroot.h header file.
 *
 * Enlightenment (0.16) can have a number of desktops with different
 * backgrounds; client windows on these are reparented, except for
 * Desktop 0 which is the root window. Therefore versions less than
 * 2.1 of xpenguins worked on Desktop 0 but not on any others. To fix
 * this we look for a root-window property _WIN_WORKSPACE which
 * contains the numerical index of the currently active desktop. The
 * active desktop is then simply the immediate child of the root
 * window that has a property ENLIGHTENMENT_DESKTOP set to this value.
 *
 * KDE 2.0: Oh dear. The kdesktop is a program separate from the
 * window manager that launches a window which sits behind all the
 * other client windows and has all the icons on it. Thus the other
 * client windows are still children of the root window, but we want
 * to draw to the uppermost window of the kdesktop. This is difficult
 * to find - it is the great-great-grandchild of the root window and
 * in KDE 2.0 has nothing to identify it from its siblings other than
 * its size. KDE 2.1+ usefully implements the __SWM_VROOT property in
 * a child of the root window, but the client windows are still
 * children of the root window. A problem is that the penguins erase
 * the desktop icons when they walk which is a bit messy. The icons
 * are not lost - they reappear when the desktop window gets an expose
 * event (i.e. move some windows over where they were and back again).
 *
 * Nautilus (GNOME 1.4+): Creates a background window to draw icons
 * on, but does not reparent the client windows. The toplevel window
 * of the desktop is indicated by the root window property
 * NAUTILUS_DESKTOP_WINDOW_ID, but then we must descend down the tree
 * from this toplevel window looking for subwindows that are the same
 * size as the screen. The bottom one is the one to draw to. Hopefully
 * one day Nautilus will implement __SWM_VROOT in exactly the same way
 * as KDE 2.1+.
 *
 * Other cases: CDE, the common desktop environment. This is a
 * commercial product that has been packaged with Sun (and other)
 * workstations. It typically implements four virtual desktops but
 * provides NO properties at all for apps such as xpenguins to use to
 * work out where to draw to. Seeing as Sun are moving over to GNOME,
 * CDE use is on the decline so I don't have any current plans to try
 * and get xpenguins to work with it.
 *
 * As a note to developers of window managers and big screen hoggers
 * like kdesktop, please visit www.freedesktop.org and implement their
 * Extended Window Manager Hints spec that help pagers and apps like
 * xpenguins and xearth to find their way around. In particular,
 * please use the _NET_CURRENT_DESKTOP and _NET_VIRTUAL_ROOTS
 * properties if you reparent any windows (e.g. Enlightenment). Since
 * no window managers that I know yet use these particular hints, I
 * haven't yet added any code to parse them.  */


#include "toon.h"
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xproto.h>
#include <stdio.h>
#include <string.h>

/* We use a common algorithm to search complex desktop window heirarchies.
 * The following structures store the data that is specific to each desktop.
 */

/* Matching rules */
typedef struct {
  char *name;
} ToonMatchRuleName;
typedef struct {
  char *class_name;
} ToonMatchRuleClassName;
typedef struct {
  char *atom_name;
  char *value;
} ToonMatchRuleProperty;
typedef struct {
  char can_be_greater;
} ToonMatchRuleSize;
typedef struct {
  char must_match;
} ToonMatchRuleDesktop;
/* A common structure which can optionally have all of the above */
typedef struct __ToonMatchRule ToonMatchRule;
struct __ToonMatchRule {
  ToonMatchRuleName *name;
  ToonMatchRuleClassName *class_name;
  ToonMatchRuleProperty *property;
  ToonMatchRuleSize *size;
  ToonMatchRuleDesktop *desktop;
};
/* A window matcher has a list of rules */
typedef struct __ToonMatchWindow ToonMatchWindow;
struct __ToonMatchWindow {
  int num_rules;
  ToonMatchRule *rules;
};
/* A desktop matcher describes the heirarchy of windows find */
typedef struct {
  int depth;
  ToonMatchWindow *windows;
  char *name;
  char can_clear; /* Does XClearArea reveal the background */
} ToonMatcher;

/* Handy macros for concisely describing the heirarchy of a desktop */
#define ARRAY_SIZE(a) (sizeof(a)/sizeof((a)[0]))
#define TOON_MATCH_WINDOW(rules) { ARRAY_SIZE(rules), rules }
#define TOON_MATCHER(wins, name, clear) { ARRAY_SIZE(wins), wins, name, clear }
/* Defines a function for performing the search algorithm for a desktop */
#define TOON_MATCH_FUNC(func_name, match) \
  static Window func_name(Display *display, int screen, \
			  unsigned long *current_desktop, Window window) \
  { \
    ToonMatcher *matcher = (match); \
    int fix_to_desktop = 0; \
    Window result = __ToonGetDesktop(display, screen, current_desktop, window, \
				     matcher, 0, &fix_to_desktop); \
    if (result) { \
      snprintf(toon_message, TOON_MESSAGE_LENGTH, \
               _("Drawing to %s desktop"), matcher->name); \
      toon_message[TOON_MESSAGE_LENGTH-1] = '\0'; \
      if (!matcher->can_clear) { \
        toon_bgcapture = 1; \
      } \
      toon_fix_to_desktop = fix_to_desktop; \
      if (current_desktop) { \
        toon_desktop = *current_desktop; \
      } \
    } \
    return result; \
  }

/* Finds the desktop window using a particular matching structure.
 * Start depth at 0 with the children of the root window.
 */
static Window
__ToonGetDesktop(Display *display, int screen, unsigned long *current_desktop,
		 Window window, ToonMatcher *match, int depth,
		 int *fix_to_desktop)
{
  Window result = (Window)0;
  ToonMatchWindow *win_match = &match->windows[depth];
  int i;
  ToonMatchRule *rule = win_match->rules;
  int w = DisplayWidth(display, screen);
  int h = DisplayHeight(display, screen);
  int go_deeper = 1;
  unsigned long nitems, bytesafter;
  Atom actual_type;
  int actual_format;

  /* Check this window against the rules */
  for (i = 0; i < win_match->num_rules; ++i, ++rule) {
    if (rule->name) {
      char *tmp_name;
      if (XFetchName(display, window, &tmp_name)) {
        if (!rule->name->name || strcasecmp(rule->name->name, tmp_name)) {
          go_deeper = 0;
        }
        XFree(tmp_name);
      }
      else if (rule->name->name) {
        go_deeper = 0;
      }
    }
    if (rule->property) {
      Atom atom = XInternAtom(display, 
                              rule->property->atom_name,
                              False);
      Atom *prop_val = NULL;
      if (XGetWindowProperty(display, window, atom, 0, 1,
                             False, XA_ATOM,
                             &actual_type, &actual_format,
                             &nitems, &bytesafter,
                             (unsigned char **) &prop_val) == Success
          && prop_val) {
        char *tmpatomname = XGetAtomName(display, *prop_val);
        if (tmpatomname) {
          if (strcmp(rule->property->value, tmpatomname)) {
            go_deeper = 0;
          }
          XFree((char *) tmpatomname);
        }
        XFree((char *) prop_val);
      }
      else {
        go_deeper = 0;
      }
    }
    if (rule->size) {
      XWindowAttributes attributes;
      if (XGetWindowAttributes(display, window, &attributes)
          && ((attributes.width < w || attributes.height < h)
              || (!rule->size->can_be_greater
                  && (attributes.width > w || attributes.height > h)))) {
        go_deeper = 0;
      }
    }
    if (rule->desktop && current_desktop) {
      Atom _NET_WM_DESKTOP = XInternAtom(display, "_NET_WM_DESKTOP", False);
      long *wm_desktop = NULL;
      if (XGetWindowProperty(display, window, _NET_WM_DESKTOP,
			     0, 1, False, XA_CARDINAL,
			     &actual_type, &actual_format,
			     &nitems, &bytesafter,
			     (unsigned char **) &wm_desktop) == Success
	  && wm_desktop) {
	int desktop = *wm_desktop;
	XFree(wm_desktop);
	if (desktop != -1) {
	  if (desktop != *current_desktop) {
	    go_deeper = 0;
	  }
	  else if (fix_to_desktop) {
	    *fix_to_desktop = 1;
	  }
	}
      }
    }
  }
  /* If no problems, go to the parent looking for the next match */
  if (go_deeper) {
    if (++depth < match->depth) {
      Window rootReturn, parentReturn, *children;
      unsigned int nChildren;
      if (XQueryTree(display, window, &rootReturn,
                     &parentReturn, &children,
                     &nChildren)) {
        int i;
        for (i = 0; i < nChildren; ++i) {
	  if ((result = __ToonGetDesktop(display, screen, current_desktop,
					 children[i], match, depth,
					 fix_to_desktop))) {
            break;
          }
        }
        XFree((char *) children);
      }
    }
    else {
      result = window;
    }
  }
  return result;
}

/* Common matching rules */

static ToonMatchRuleName noname_rule = { NULL };
static ToonMatchRuleSize size_rule = { 1 };
static ToonMatchRuleDesktop desktop_rule = { 1 };
static ToonMatchRuleProperty desktop_type_rule
  = { "_NET_WM_WINDOW_TYPE", "_NET_WM_WINDOW_TYPE_DESKTOP" };

/* KDE 4
 * -> The root window
 * 0 -> window with no name
 * 1   -> window with no name
 * 2     -> window with name="plasma-desktop" & _NET_WM_WINDOW_TYPE_DESKTOP
 * 			& matching desktop number
 */

static ToonMatchRuleName kde4_name_rule = { "plasma-desktop" };
static ToonMatchRule kde4_match_rules_0_1[]
  = { { &noname_rule, NULL, NULL, &size_rule } };
static ToonMatchRule kde4_match_rules_2[]
  = { { &kde4_name_rule, NULL, &desktop_type_rule,
	&size_rule, &desktop_rule } };
static ToonMatchWindow kde4_matcher_windows[] = {
  TOON_MATCH_WINDOW(kde4_match_rules_0_1),
  TOON_MATCH_WINDOW(kde4_match_rules_0_1),
  TOON_MATCH_WINDOW(kde4_match_rules_2)
};
static ToonMatcher kde4_matcher = TOON_MATCHER(kde4_matcher_windows,
                                               "KDE 4", 0);
TOON_MATCH_FUNC(__ToonGetKDE4Desktop, &kde4_matcher)

/* KDE 3
 * -> The root window
 * 0 -> window with no name
 * 1   -> window with no name
 * 2     -> window with name="KDE Desktop" & _NET_WM_WINDOW_TYPE_DESKTOP
 * 3       -> window with no name and width >= width of screen
 * 4         -> window with no name and width >= width of screen
 */

static ToonMatchRuleName kde3_name_rule = { "KDE Desktop" };
static ToonMatchRule kde3_match_rules_0_1_3_4[]
  = { { &noname_rule, NULL, NULL, &size_rule } };
static ToonMatchRule kde3_match_rules_2[]
  = { { &kde3_name_rule, NULL, &desktop_type_rule, &size_rule } };
static ToonMatchWindow kde3_matcher_windows[] = {
  TOON_MATCH_WINDOW(kde3_match_rules_0_1_3_4),
  TOON_MATCH_WINDOW(kde3_match_rules_0_1_3_4),
  TOON_MATCH_WINDOW(kde3_match_rules_2),
  TOON_MATCH_WINDOW(kde3_match_rules_0_1_3_4),
  TOON_MATCH_WINDOW(kde3_match_rules_0_1_3_4)
};
static ToonMatcher kde3_matcher = TOON_MATCHER(kde3_matcher_windows,
                                               "KDE 3", 1);
TOON_MATCH_FUNC(__ToonGetKDE3Desktop, &kde3_matcher)

/* KDE 2
 * It works with KDE 2.0, but since KDE 2.0 is less stable
 * than Windows 95, I don't expect many people to remain using it now
 * that 2.1 is available, which implements __SWM_VROOT and makes this
 * function redundant. This is the hierarchy we're trying to traverse:
 *
 * -> The root window
 * 0 -> window with name="KDE Desktop"
 * 1   -> window with no name
 * 2     -> window with name="KDE Desktop" & _NET_WM_WINDOW_TYPE_DESKTOP
 * 3       -> window with no name and width >= width of screen
 */

static ToonMatchRuleName kde2_name_rule = { "KDE Desktop" };
static ToonMatchRule kde2_match_rules_0[]
  = { { &kde2_name_rule, NULL, NULL, &size_rule } };
static ToonMatchRule kde2_match_rules_1_3[]
  = { { &noname_rule, NULL, NULL, &size_rule } };
static ToonMatchRule kde2_match_rules_2[]
  = { { &kde2_name_rule, NULL, &desktop_type_rule, &size_rule } };
static ToonMatchWindow kde2_matcher_windows[] = {
  TOON_MATCH_WINDOW(kde2_match_rules_0),
  TOON_MATCH_WINDOW(kde2_match_rules_1_3),
  TOON_MATCH_WINDOW(kde2_match_rules_2),
  TOON_MATCH_WINDOW(kde2_match_rules_1_3)
};
static ToonMatcher kde2_matcher = TOON_MATCHER(kde2_matcher_windows,
                                               "KDE 2", 1);
TOON_MATCH_FUNC(__ToonGetKDE2Desktop, &kde2_matcher)

/* Look for the Nautilus desktop window to draw to, given the toplevel
 * window of the Nautilus desktop. Basically recursively calls itself
 * looking for subwindows the same size as the root window. */
static
Window
__ToonGetNautilusDesktop(Display *display, int screen, Window window,
			 int depth)
{
  Window rootReturn, parentReturn, *children;
  Window winreturn = window;
  unsigned int nChildren;

  if (depth > 5) {
    return ((Window) 0);
  }
  else if (XQueryTree(display, window, &rootReturn, &parentReturn,
		 &children, &nChildren)) {
    int i;
    for (i = 0; i < nChildren; ++i) {
      XWindowAttributes attributes;
      if (XGetWindowAttributes(display, children[i], &attributes)) {
	if (attributes.width == DisplayWidth(display, screen)
	    && attributes.height == DisplayHeight(display, screen)) {
	  /* Found a possible desktop window */
	  winreturn = __ToonGetNautilusDesktop(display, screen,
					       children[i], depth+1);
	}
      }  
    }
    XFree((char *) children);
  }
  return winreturn;
}


/* 
 * Returns the window ID of the `background' window on to which the
 * toons should be drawn. Also returned (in clientparent) is the ID of
 * the parent of all the client windows, since this may not be the
 * same as the background window. If no recognised virtual window
 * manager or desktop environment is found then the root window is
 * returned in both cases. The string toon_message contains
 * information about the window manager that was found.
 */
Window
ToonGetRootWindow(Display *display, int screen, Window *clientparent)
{
  Window background = 0; /* The return value */
  Window root = RootWindow(display, screen);
  Window rootReturn, parentReturn, *children;
  Window *toplevel = (Window *) 0;
  unsigned int nChildren;
  unsigned long nitems, bytesafter;
  Atom actual_type;
  int actual_format;
  unsigned long *workspace = NULL;
  unsigned long *desktop = NULL;
  Atom NAUTILUS_DESKTOP_WINDOW_ID = XInternAtom(display,
			   "NAUTILUS_DESKTOP_WINDOW_ID",
						False);

  *clientparent = root;

  if (XGetWindowProperty(display, root,
			 NAUTILUS_DESKTOP_WINDOW_ID,
			 0, 1, False, XA_WINDOW,
			 &actual_type, &actual_format,
			 &nitems, &bytesafter,
			 (unsigned char **) &toplevel) == Success
      && toplevel) {
    /* Nautilus is running */
    background = __ToonGetNautilusDesktop(display, screen,
					  *toplevel, 0);
    XFree((char *) toplevel);
    if (background) {
      snprintf(toon_message, TOON_MESSAGE_LENGTH,
	       _("Drawing to Nautilus Desktop"));
      toon_message[TOON_MESSAGE_LENGTH-1] = '\0';
    }
  }

  /* Next look for a virtual root or a KDE Desktop */
  if (!background
      && XQueryTree(display, root, &rootReturn, &parentReturn,
		    &children, &nChildren)) {
    int i;
    Atom __SWM_VROOT = XInternAtom(display, "__SWM_VROOT", False);
    Atom _NET_CURRENT_DESKTOP = XInternAtom(display, "_NET_CURRENT_DESKTOP",
					    False);
    unsigned long *current_desktop = NULL;
    if (XGetWindowProperty(display, root, _NET_CURRENT_DESKTOP,
			   0, 1, False, XA_CARDINAL,
			   &actual_type, &actual_format,
			   &nitems, &bytesafter,
			   (unsigned char **) &current_desktop) != Success) {
      current_desktop = NULL;
    }

    for (i = 0; i < nChildren && !background; ++i) {
      Window *newroot = (Window *) 0;
      if (XGetWindowProperty(display, children[i],
			     __SWM_VROOT, 0, 1, False, XA_WINDOW,
			     &actual_type, &actual_format,
			     &nitems, &bytesafter,
			     (unsigned char **) &newroot) == Success
	  && newroot) {
	/* Found a window with a __SWM_VROOT property that contains
	 * the window ID of the virtual root. Now we must check
	 * whether it is KDE (2.1+) or not. If it is KDE then it does
	 * not reparent the clients. If the root window has the
	 * _NET_SUPPORTED property but not the _NET_VIRTUAL_ROOTS
	 * property then we assume it is KDE. */
	Atom _NET_SUPPORTED = XInternAtom(display,
					  "_NET_SUPPORTED",
					  False);
	Atom *tmpatom;
	if (XGetWindowProperty(display, root,
			       _NET_SUPPORTED, 0, 1, False,
			       XA_ATOM, &actual_type, &actual_format,
			       &nitems, &bytesafter,
			       (unsigned char **) &tmpatom) == Success
	    && tmpatom) {
	  Window *tmpwindow = (Window *) 0;
	  Atom _NET_VIRTUAL_ROOTS = XInternAtom(display,
						"_NET_VIRTUAL_ROOTS",
						False);
	  XFree((char *) tmpatom);
	  if (XGetWindowProperty(display, root,
				 _NET_VIRTUAL_ROOTS, 0, 1, False,
				 XA_WINDOW, &actual_type, &actual_format,
				 &nitems, &bytesafter,
				 (unsigned char **) &tmpwindow) != Success
	      || !tmpwindow) {
	    /* Must be KDE 2.1+ */
	    snprintf(toon_message, TOON_MESSAGE_LENGTH,
		     _("Drawing to KDE 2 Desktop"));
	    toon_message[TOON_MESSAGE_LENGTH-1] = '\0';
	    background = *newroot;
	  }
	  else if (tmpwindow) {
	    XFree((char *) tmpwindow);
	  }
	}

	if (!background) {  
	  /* Not KDE: assume windows are reparented */
	  snprintf(toon_message, TOON_MESSAGE_LENGTH,
		   _("Drawing to virtual root window"));
	  toon_message[TOON_MESSAGE_LENGTH-1] = '\0';
	  background = *clientparent = *newroot;
	}
	XFree((char *) newroot);
      }
      else if ((background = __ToonGetKDE4Desktop(display, screen,
						  current_desktop,
						  children[i]))) {
	/* Found a KDE 4 plasma desktop and located the background window */
	/* Note that the clientparent is still the root window */
      }
      else if ((background = __ToonGetKDE3Desktop(display, screen,
						  current_desktop,
						  children[i]))) {
	/* Found a KDE 3 desktop and located the background window */
	/* Note that the clientparent is still the root window */
      }
      else if ((background = __ToonGetKDE2Desktop(display, screen,
						  current_desktop,
						  children[i]))) {
	/* Found a KDE 2.0 desktop and located the background window */
	/* Note that the clientparent is still the root window */
      }
    }
    XFree((char *) children);
    if (current_desktop) {
      XFree((char *) current_desktop);
    }
  }

  if (!background) {
    /* Look for a _WIN_WORKSPACE property, used by Enlightenment */
    Atom _WIN_WORKSPACE = XInternAtom(display, "_WIN_WORKSPACE", False);
    if (XGetWindowProperty(display, root, _WIN_WORKSPACE,
			   0, 1, False, XA_CARDINAL,
			   &actual_type, &actual_format,
			   &nitems, &bytesafter,
			   (unsigned char **) &workspace) == Success
	&& workspace) {
      /* Found a _WIN_WORKSPACE property - this is the desktop to look for.
       * For now assume that this is Enlightenment.
       * We're looking for a child of the root window that has an
       * ENLIGHTENMENT_DESKTOP atom with a value equal to the root window's
       * _WIN_WORKSPACE atom. */
      Atom ENLIGHTENMENT_DESKTOP = XInternAtom(display, 
					       "ENLIGHTENMENT_DESKTOP",
					       False);
      /* First check to see if the root window is the current desktop... */
      if (XGetWindowProperty(display, root,
			     ENLIGHTENMENT_DESKTOP, 0, 1,
			     False, XA_CARDINAL,
			     &actual_type, &actual_format,
			     &nitems, &bytesafter,
			     (unsigned char **) &desktop) == Success
	  && desktop && *desktop == *workspace) {
	/* The root window is the current Enlightenment desktop */
	snprintf(toon_message, TOON_MESSAGE_LENGTH,
		 _("Drawing to Enlightenment Desktop %lu (the root window)"),
		 *desktop);
	toon_message[TOON_MESSAGE_LENGTH-1] = '\0';
	background = root;
	XFree((char *) desktop);
      }
      /* Now look at each immediate child window of root to see if it is
       * the current desktop */
      else if (XQueryTree(display, root, &rootReturn, &parentReturn,
			  &children, &nChildren)) {
	int i;
	for (i = 0; i < nChildren; ++i) {
	  if (XGetWindowProperty(display, children[i],
				 ENLIGHTENMENT_DESKTOP, 0, 1,
				 False, XA_CARDINAL,
				 &actual_type, &actual_format,
				 &nitems, &bytesafter,
				 (unsigned char **) &desktop) == Success
	      && desktop && *desktop == *workspace) {
	    /* Found current Enlightenment desktop */
	    snprintf(toon_message, TOON_MESSAGE_LENGTH,
		     _("Drawing to Enlightenment Desktop %lu"),
		     *desktop);
	    toon_message[TOON_MESSAGE_LENGTH-1] = '\0';
	    background = *clientparent = children[i];
	    XFree((char *) desktop);
	  }
	}
	XFree((char *) children);
      }
      XFree((char *) workspace);
    }
  }
  if (background) {
    return background;
  }
  else {
    toon_message[0] = '\0';
    return root;
  }
}
